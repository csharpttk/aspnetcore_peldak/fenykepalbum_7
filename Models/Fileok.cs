﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;

namespace fenykepalbum_7_1.Models
{
    public class Fájl
    {
        public string filenév { get; set; } 
        public DateTime feltöltésideje { get; set; }
        public Fájl(string filenév,DateTime feltöltésideje)
        {
            this.filenév = filenév;
            this.feltöltésideje = feltöltésideje;
        }
        public Fájl(string filenév)
        {
            this.filenév = filenév;
           
        }
     
        public bool Töröl()
        {
            try
            {
                File.Delete("wwwroot/kepek/" + this.filenév);
                return true;
            }
            catch (Exception ex)
            { return false; }
        }
    }
    public class Fileok
    {     
        public List<string> fnevek { get;set; }
        public List<Fájl> fnevekidők { get; set; }
        public Fileok(string útvonal)
        {
            fnevek = Directory.EnumerateFiles(útvonal).Select(x => x.Substring(útvonal.Length)).ToList();       
        }
        public Fileok(string útvonal,string rendezés)
        { var rendezett = Directory.EnumerateFiles(útvonal);
          var fileok = rendezett.Select(x => new Fájl(  x.Substring(útvonal.Length), Directory.GetCreationTime(x)));
          var fileokdinamikus = fileok.OrderBy(x => x.feltöltésideje).AsQueryable();
          fnevekidők = fileokdinamikus.OrderBy(rendezés).ToList();
        }
    }
}
