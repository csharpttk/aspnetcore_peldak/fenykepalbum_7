﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace fenykepalbum_7_1.AspNetCore.NewDb.Models
{
    public class fenykepContext : DbContext
    {
        public fenykepContext(DbContextOptions<fenykepContext> options)
            : base(options)
        { }
        public DbSet<Kepek>Kepek { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Kepek>().HasData(new Kepek
            {
                Id = 1,
                Filenev = "WP_20150531_15_50_32_Pro.jpg",
                Holkeszult = "Lednice",
                Mikor = new DateTime(2015, 5, 31),
                Leiras = "Kastély park, tó"

            }, new Kepek
            {
                Id = 2,
                Filenev = "WP_20150531_16_01_58_Pro.jpg",
                Holkeszult = "Lednice",
                Mikor = new DateTime(2015, 5, 31),
                Leiras = "Kastély park, tó"
            }
            , new Kepek
            {
                Id = 3,
                Filenev = "WP_20161124_14_22_05_Pro.jpg",
                Holkeszult = "ÓTátrafüred",
                Mikor = new DateTime(2016, 11, 24),
                Leiras = "Felhőbe burkolózó hegycsúcs"
            }
            , new Kepek
            {
                Id = 4,
                Filenev = "WP_20171025_14_09_38_Pro.jpg",
                Holkeszult = "Tátralomnic",
                Mikor = new DateTime(2017, 10, 25),
                Leiras = "Hegycsúcs"
            }
            , new Kepek
            {
                Id = 5,
                Filenev = "WP_20171025_14_10_37_Pro.jpg",
                Holkeszult = "Tátralomnic",
                Mikor = new DateTime(2017, 10, 25),
                Leiras = "Kilátás a hegyről"
            }
            , new Kepek
            {
                Id = 6,
                Filenev = "WP_20171025_14_23_14_Pro.jpg",
                Holkeszult = "Tátralomnic",
                Mikor = new DateTime(2017, 10, 25),
                Leiras = "Lanovka"
            }, new Kepek
            {
                Id = 7,
                Filenev = "WP_20171025_14_35_07_Pro.jpg",
                Holkeszult = "Tátralomnic",
                Mikor = new DateTime(2017, 10, 25),
                Leiras = "Hegyi tó"
            }, new Kepek
            {
                Id = 8,
                Filenev = "WP_20180806_10_34_31_Pro.jpg",
                Holkeszult = "Capri",
                Mikor = new DateTime(2018, 8, 6),
                Leiras = "Kilátás a hegyről"
            }, new Kepek
            {
                Id = 9,
                Filenev = "WP_20180806_11_10_49_Pro.jpg",
                Holkeszult = "Capri",
                Mikor = new DateTime(2018, 8, 6),
                Leiras = "Kilátás a hegyről"
            }, new Kepek
            {
                Id = 10,
                Filenev = "WP_20180806_12_05_57_Pro.jpg",
                Holkeszult = "Capri",
                Mikor = new DateTime(2018, 8, 6),
                Leiras = "Kilátás a hegyről, sziklák"
               
            }, new Kepek
            {
                Id = 11,
                Filenev = "WP_20180807_16_02_43_Pro.jpg",
                Holkeszult = "Sorrento",
                Mikor = new DateTime(2018, 8, 7),
                Leiras = "Kilátás az öbölre"
            }, new Kepek
            {
                Id = 12,
                Filenev = "WP_20180809_12_00_31_Pro.jpg",
                Holkeszult = "Amalfi",
                Mikor = new DateTime(2018, 8, 9),
                Leiras = "Tenger és hegyoldal"
            }, new Kepek
            {
                Id = 13,
                Filenev = "WP_20180809_14_00_53_Pro.jpg",
                Holkeszult = "Amalfi",
                Mikor = new DateTime(2018, 8, 9),
                Leiras = "Hegyoldal"
            }, new Kepek
            {
                Id = 14,
                Filenev = "WP_20180811_15_42_18_Pro.jpg",
                Holkeszult = "Róma",
                Mikor = new DateTime(2018, 8, 11),
                Leiras = "Angyalvár"
            }, new Kepek
            {
                Id = 15,
                Filenev = "WP_20180811_19_16_55_Pro.jpg",
                Holkeszult = "Róma",
                Mikor = new DateTime(2018, 8, 11),
                Leiras = "Colosseum"
            }
            )     ;
        }
        
    }

    public class Kepek
    {
        public int Id { get; set; }

        [Display(Name = "Fájl neve")]
        public string Filenev { get; set; }
        [Display(Name = "Hol készült")]
        public string Holkeszult { get; set; }
        [Display(Name = "Mikor")]
        public DateTime Mikor { get; set; }
        [Display(Name = "Leírás")]
        public string Leiras { get; set; }
    }
}