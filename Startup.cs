﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using fenykepalbum_7_1.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using fenykepalbum_7_1.AspNetCore.NewDb.Models;
using fenykepalbum_7_1.Models;

namespace fenykepalbum_7_1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<IdentityUser,IdentityRole>()
                .AddDefaultUI(UIFramework.Bootstrap4)
                .AddEntityFrameworkStores<ApplicationDbContext>();
                //.AddErrorDescriber<Hibaüzenetek>()
           

            var connection = @"Server=(localdb)\mssqllocaldb;Database=fenykepalbum_7.AspNetCore.NewDb;Trusted_Connection=True;ConnectRetryCount=0";
            services.AddDbContext<fenykepContext>
               (options => options.UseSqlServer(connection));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
        private async Task AdminLétrehozás(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            //Adding Admin Role
            var vanmárAdmin = await RoleManager.RoleExistsAsync("admin");
            if (!vanmárAdmin)
            {
                IdentityRole szerepkör = new IdentityRole("admin");
                await RoleManager.CreateAsync(szerepkör);
                //create the roles and seed them to the database
              
            }
           
            UserManager<IdentityUser> userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            IdentityUser admin = await userManager.FindByNameAsync("admin") ;
            if (admin == null)
            {   var user = new IdentityUser { UserName = "admin@inf.elte.hu", Email="admin@inf.elte.hu" , EmailConfirmed=true, LockoutEnabled=false};
                var sikerültelkészíteni = await userManager.CreateAsync(user, "JelszavaM_123");
                if (sikerültelkészíteni.Succeeded)
                {
                    var sikerülthozzáadni = await userManager.AddToRoleAsync(user, "admin");
                }
            }
            //Assign Admin role to the main User here we have given our newly registered 
            //login id for Admin management

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            AdminLétrehozás(serviceProvider).Wait();
        }
    }
}
